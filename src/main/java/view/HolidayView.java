package view;

import model.Holiday;

import java.util.*;

public class HolidayView {

  public void findMenu() {
    System.out.println("Welcome to Holidays club find menu\n");
    System.out.println("By which parameter do yo wanna find the holidays?\n");
    System.out.println("1)By price\n2)By avalibility\n3)By location\n4)exit " +
            "find menu");
  }

  public void sortMenu() {
    System.out.println("Welcome to Holidays club sort menu\n");
    System.out.println("By which parameter do yo wanna sort the holidays?\n");
    System.out.println("1)By price\n2)By duration\n3)exit sort menu");
  }

  public void printMenu() {
    System.out.println("Welcome to Holidays club\n");
    System.out.println("Take action : \n");
    System.out.println(
        "1)Open find menu\n2)Print holidays\n3)Sort holidays menu\n" +
                "4)Delete by name\n5)exit");
  }

  public void printHolidays(List<Holiday> holidays) {
    for (Holiday holiday : holidays) {
      System.out.println(holiday.toString());
    }
  }
}
