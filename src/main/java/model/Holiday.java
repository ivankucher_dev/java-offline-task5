package model;

public class Holiday {
  private String name;
  private String location;
  private boolean avalibility;
  private double cost;
  private double duration;

  public Holiday(String name, String location, boolean avalibility,
                 double cost, double duration) {
    this.name = name;
    this.location = location;
    this.avalibility = avalibility;
    this.cost = cost;
    this.duration = duration;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public boolean isAvalibility() {
    return avalibility;
  }

  public void setAvalibility(boolean avalibility) {
    this.avalibility = avalibility;
  }

  public double getCost() {
    return cost;
  }

  public void setCost(double cost) {
    this.cost = cost;
  }

  public double getDuration() {
    return duration;
  }

  public void setDuration(double duration) {
    this.duration = duration;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("Holiday{");
    sb.append("Name=").append(name);
    sb.append(", price=").append(cost);
    sb.append(", location='").append(location).append('\'');
    sb.append(", avalibility=").append(avalibility);
    sb.append(", duration=").append(duration);
    sb.append("}\n");
    return sb.toString();
  }
}
