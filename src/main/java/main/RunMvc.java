package main;

import controller.HolidayController;
import model.Holiday;
import view.HolidayView;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class RunMvc {

  private static final int OPEN_FIND_MENU = 1;
  private static final int PRINT_HOLIDAYS = 2;
  private static final int SORT_MENU = 3;
  private static final int DELETE_BY_NAME = 4;
  private static final int FIND_BY_AVALIBILITY = 2;
  private static final int FIND_BY_PRICE = 1;
  private static final int FIND_BY_LOCATION = 3;
  private static final int EXIT_FROM_FIND_MENU = 4;
  private static final int EXIT_FROM_SORT_MENU = 3;
  private static final int SORT_BY_DURATION = 2;
  private static final int SORT_BY_PRICE = 1;

  private ArrayList<Holiday> model;
  private HolidayView view;
  private HolidayController controller;

  public RunMvc() {
    model = retrieveDataFromDatabase();
    view = new HolidayView();
    controller = new HolidayController(model, view);
  }

  public void run() {
    int input;

    do {
      view.printMenu();
      input = chooser(1, 5);
      switch (input) {
        case OPEN_FIND_MENU:
          view.findMenu();
          readFromFindMenu();
          break;

        case PRINT_HOLIDAYS:
          view.printHolidays(controller.getModel());
          System.out.println("enter any number to continue.....");
          chooser(0, 9);
          break;
        case SORT_MENU:
          view.sortMenu();
          readInputFromSortMenu();
          break;

        case DELETE_BY_NAME:
          deleteByName();
          break;
      }
    } while (input != 5);
  }

  private void readInputFromSortMenu() {
    int input;
    do {
      input = chooser(1, 3);
      switch (input) {
        case SORT_BY_PRICE:
          controller.sorByCost();
          break;
        case SORT_BY_DURATION:
          controller.sortByDuration();
          break;
      }
    } while (input != EXIT_FROM_SORT_MENU);
  }

  private void readFromFindMenu() {
    Scanner scanner = new Scanner(System.in);
    int input;
    do {
      input = chooser(1, 4);
      switch (input) {
        case FIND_BY_PRICE:
          System.out.println("Enter cost : ");
          double cost = scanner.nextDouble();
          controller.findByCost(cost);
          break;
        case FIND_BY_AVALIBILITY:
          System.out.println("Enter avalibility : ");
          boolean avalibility = scanner.nextBoolean();
          controller.findByAvalibility(avalibility);
          break;
        case FIND_BY_LOCATION:
          System.out.println("Enter location : \n");
          String location = scanner.nextLine();
          controller.findByLocation(location);
          break;
      }
    } while (input != EXIT_FROM_FIND_MENU);
  }

  private void deleteByName() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter name : \n");
    String name = scanner.nextLine();
    controller.deleteByName(name);
  }

  private int chooser(int firstLimit, int secondLimit) {

    Scanner scanner = new Scanner(System.in);
    int choise = -1;
    while (choise < firstLimit || choise > secondLimit) {
      try {
        System.out.println("Your choise : ");
        choise = scanner.nextInt();
      } catch (InputMismatchException e) {
        System.out.println("Input number must be like in menu!");
        scanner.next();
        continue;
      }
    }

    return choise;
  }

  public static ArrayList<Holiday> retrieveDataFromDatabase() {
    ArrayList<Holiday> holidays = new ArrayList<Holiday>();
    holidays.add(new Holiday("Summer time", "forest", true, 150, 48));
    holidays.add(new Holiday("Childrens day", "city", false, 245, 24));
    holidays.add(new Holiday("River jump", "river", true, 45, 4));
    holidays.add(new Holiday("Bunny play", "building", true, 100, 6));
    holidays.add(new Holiday("Sea playing", "sea", false, 445, 72));
    holidays.add(new Holiday("Water", "rooms and city", true, 96, 8));
    holidays.add(new Holiday("Dumping", "rooms", true, 106, 12));
    return holidays;
  }
}
