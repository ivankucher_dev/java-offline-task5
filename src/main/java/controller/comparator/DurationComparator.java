package controller.comparator;

import model.Holiday;

import java.util.Comparator;

public class DurationComparator implements Comparator<Holiday> {

  public int compare(Holiday o1, Holiday o2) {
    return (int) (o1.getDuration() - o2.getDuration());
  }
}
