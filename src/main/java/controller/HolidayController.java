package controller;

import controller.comparator.DurationComparator;
import controller.comparator.PriceComparator;
import model.Holiday;
import view.HolidayView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HolidayController implements BasicHolidayController {

  private List<Holiday> model;
  private HolidayView view;

  public List<Holiday> getModel() {
    return model;
  }

  public HolidayView getView() {
    return view;
  }

  public HolidayController(List<Holiday> model, HolidayView view) {
    this.model = model;
    this.view = view;
  }

  public void deleteByName(String name) {
    Holiday holiday = findByName(name);
    if (holiday != null) {
      model.remove(holiday);
      updateView();
    }
  }

  public List<Holiday> findByAvalibility(boolean avalibility) {
    ArrayList<Holiday> holidays = new ArrayList<Holiday>();
    for (Holiday holiday : model) {
      if (holiday.isAvalibility() == avalibility) holidays.add(holiday);
    }
    view.printHolidays(holidays);
    return holidays;
  }

  public List<Holiday> findByCost(double cost) {
    ArrayList<Holiday> holidays = new ArrayList<Holiday>();
    for (Holiday holiday : model) {
      if (holiday.getCost() == cost) holidays.add(holiday);
    }
    view.printHolidays(holidays);
    return holidays;
  }

  public List<Holiday> findByLocation(final String location) {
    ArrayList<Holiday> holidays = new ArrayList<Holiday>();
    for (Holiday holiday : model) {
      if (holiday.getLocation().equals(location)) holidays.add(holiday);
    }
    view.printHolidays(holidays);
    return holidays;
  }

  public Holiday findByName(String name) {
    for (Holiday holiday : model) {
      if (holiday.getName().equals(name)) return holiday;
    }
    return null;
  }

  public void sortByDuration() {
    Collections.sort(model, new DurationComparator());
    updateView();
  }

  public void sorByCost() {
    Collections.sort(model, new PriceComparator());
    updateView();
  }

  public void updateView() {
    view.printHolidays(model);
  }
}
