package controller;

import model.Holiday;

import java.util.List;

public interface BasicHolidayController {
  void deleteByName(String name);

  List<Holiday> findByAvalibility(boolean avalibility);

  List<Holiday> findByCost(double cost);

  List<Holiday> findByLocation(String location);

  Holiday findByName(String name);

  void sortByDuration();

  void sorByCost();

  void updateView();
}
